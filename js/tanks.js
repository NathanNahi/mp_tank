///////////////////////////////// EURECA CODE /////////////////////////////////
var ready = false;
var gameSeed = null;
var eurecaServer;
//this function will handle client communication with the server
var eurecaClientSetup = function() {
	//create an instance of eureca.io client
	var eurecaClient = new Eureca.Client();
	
	eurecaClient.ready(function (proxy) {		
		eurecaServer = proxy;
	});

    eurecaClient.exports.setId = function(id) {
        myId = id;
        create();
        eurecaServer.handshake();
        ready = true;
    }

    eurecaClient.exports.setSeed = function(seed) {
        gameSeed = seed;
    }

    eurecaClient.exports.kill = function(id) {
        if (tanksList[id]) {
            tanksList[id].kill();
            game.time.events.add(respawnTime, () => {
                eurecaServer.revivePlayer(id);
            }, this)
            console.log('killing', id, tanksList[id]);
        }
    }

    eurecaClient.exports.revive = function(id, x, y) {
        if (tanksList[id]) {
            tanksList[id].revive();
        }
    }

    eurecaClient.exports.spawnEnemy = function(i, x, y) {
        if (i == myId) return;

        var tank = new Tank(i, game, tank);
        tank.justSpawned = true;
        tank.tank.x = x;
        tank.tank.y = y;
        tanksList[i] = tank;
    }

    eurecaClient.exports.updateState = function(id, state) {
        if (tanksList[id]) {
            tanksList[id].cursor = state;
            tanksList[id].tank.x = state.x;
            tanksList[id].tank.y = state.y;
            tanksList[id].tank.angle = state.angle;
            tanksList[id].turret.rotation = state.rot;

            tanksList[id].update();
        }
    }

    eurecaClient.exports.updateHealth = function(id, hp) {
        if (tanksList[id]) {
            tanksList[id].hp -= hp;
        }
    }
}
///////////////////////////////// GAME CODE /////////////////////////////////

var myId = 0;

var land;

var tank;
var turret;
var player;
var tanksList;
var enemyBullets;
var obstacles
var enemiesTotal = 0;
var enemiesAlive = 0;
var respawnTime = 5000;
var explosions;

var deathGraphic;

var cursors;

var bullets;
var fireRate = 100;
var nextFire = 0;


Tank = function (index, game, player, id) {
	this.cursor = {
		left:false,
		right:false,
		up:false,
        down: false,
		fire:false,
	}

	this.input = {
		left:false,
		right:false,
		up:false,
        down:false,
		fire:false
	}

    var x = 0;
    var y = 0;

    this.game = game;
    this.hpBase = 30;
    this.hp = this.hpBase;
    this.id = id;
    this.player = player;
    this.justSpawned = true;
    this.collisionDetected = false;
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.createMultiple(20, "playerBullet", 0, false);
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);
	
	this.currentSpeed = 0;
    this.fireRate = 500;
    this.nextFire = 0;
    this.alive = true;

    if (index == myId) {
        this.tank = game.add.sprite(x, y, "playerTank");
        this.turret = game.add.sprite(x, y, "playerBarrel");
    } else {
        this.tank = game.add.sprite(x, y, "enemyTank");
        this.turret = game.add.sprite(x, y, "enemyBarrel");
    }

    this.tank.anchor.set(0.5);
    this.turret.anchor.set(0.3, 0.5);

    this.tank.id = index;
    game.physics.enable(this.tank, Phaser.Physics.ARCADE);
    this.tank.body.immovable = false;
    this.tank.body.collideWorldBounds = true;
    this.tank.body.bounce.setTo(0, 0);

    this.tank.angle = 0;

    game.physics.arcade.velocityFromRotation(this.tank.rotation, 0, this.tank.body.velocity);
};

Tank.prototype.update = function() {

	var inputChanged = (
		this.cursor.left != this.input.left ||
		this.cursor.right != this.input.right ||
		this.cursor.up != this.input.up ||
        this.cursor.down != this.input.down ||
		this.cursor.fire != this.input.fire
	);
	
	if (inputChanged || this.collisionDetected || this.justSpawned)
	{
		//Handle input change here
		//send new values to the server
		if (this.tank.id == myId)
		{
			// send latest valid state to the server
			this.input.x = this.tank.x;
			this.input.y = this.tank.y;
			this.input.angle = this.tank.angle;
			this.input.rot = this.turret.rotation;

			eurecaServer.handleState(this.input);
		}

        if (this.justSpawned) {
            this.justSpawned = false;

            // handle Just spawned ...
        }

        if (this.collisionDetected) {
            this.collisionDetected = false;

            // handle collision detected ...
        }
	}

    if (this.cursor.left)
    {
        this.tank.angle -= 1;
    }
    else if (this.cursor.right)
    {
        this.tank.angle += 1;
    }	
    if (this.cursor.up)
    {
        this.currentSpeed = 300;
    }
    else if (this.cursor.down) {
        this.currentSpeed = -300;
    }
    else
    {
        this.currentSpeed = this.currentSpeed * 0.95;
    }
    if (this.cursor.fire)
    {	
		this.fire({x:this.cursor.tx, y:this.cursor.ty});
    }
	
    this.game.physics.arcade.velocityFromRotation(this.tank.rotation, this.currentSpeed, this.tank.body.velocity);

    this.turret.x = this.tank.x;
    this.turret.y = this.tank.y;
};

Tank.prototype.fire = function(target) {
    if (!this.alive) return;
    if (this.game.time.now > this.nextFire && this.bullets.countDead() > 0)
    {
        this.nextFire = this.game.time.now + this.fireRate;
        var bullet = this.bullets.getFirstDead();
        bullet.reset(this.turret.x, this.turret.y);

        bullet.rotation = this.game.physics.arcade.moveToObject(bullet, target, 500);
    }
}


Tank.prototype.kill = function() {
	this.alive = false;
	this.tank.kill();
	this.turret.kill();
    this.game.time.events.add(respawnTime, () => {
        eurecaServer.revivePlayer(this.tank.id);
    }, this);

    if (this.tank.id === myId) {
        deathGraphicShow();
    }
}

Tank.prototype.revive = function() {
    let spawnPoint = getSpawnPoint(game.world.bounds);

	this.alive = true;
    this.justSpawned = true;
    this.hp = this.hpBase;
    this.tank.x = spawnPoint.x;
    this.tank.y = spawnPoint.y;

	this.tank.revive();
	this.turret.revive();
}

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example', { preload: preload, create: eurecaClientSetup, update: update});

function preload () {
    game.load.image("playerTank", "assets/tank_body_player.png");
    game.load.image("playerBarrel", "assets/tank_barrel_player.png");
    game.load.image("playerBullet", "assets/bullet_player.png");
    game.load.image("enemyTank", "assets/tank_body_enemy.png");
    game.load.image("enemyBarrel", "assets/tank_barrel_enemy.png");
    game.load.image("enemyBullet", "assets/bullet_enemy.png");
    game.load.image("deathGraphic", "assets/death_graphic.png");
    game.load.image("grid", "assets/grid.png");
    game.load.spritesheet("explosion", "assets/explosion.png", 64, 64, 8);
}

function generateObstacles() {
    console.log(gameSeed);
    // Obstacle pool ...
    obstacles = game.add.group();
    for (var i = 0; i < 25; i ++) {
        let shape;
        let shapeSprite;
        let graphic;
        let rot;
        let maxWidth  = 100;
        let minWidth  = 10;
        let maxHeight = 100;
        let minHeight = 10;

        shape = new Phaser.Rectangle(
            Math.random() * game.world.bounds.width,
            Math.random() * game.world.bounds.height,
            Phaser.Math.clamp(Math.random() * maxWidth, minWidth, maxWidth),
            Phaser.Math.clamp(Math.random() * maxHeight, minHeight, maxHeight),
        );
        graphics = game.make.graphics();
        graphics.beginFill(0xE4E4E3);
        graphics.drawRect(
            0,
            0,
            shape.width,
            shape.height,
        );

        shapeSprite = game.make.sprite(shape.x, shape.y, graphics.generateTexture());
        game.physics.enable(shapeSprite, Phaser.Physics.ARCADE);
        shapeSprite.anchor.setTo(0.5);
        shapeSprite.body.enable = true;
        shapeSprite.body.immovable = true;
        rot = (2 * Math.PI) * Math.random();
        shapeSprite.rotation = rot;
        shapeSprite.body.rotation = rot;

        obstacles.add(shapeSprite);
    }
}

function getSpawnPoint() {
    return new Phaser.Point(
        Math.random() * game.world.bounds.width,
        Math.random() * game.world.bounds.height,
    );
}

function create () {
    // Probably a lot of shit that should be defined server side but
    // for this case we'll just ignore that ...

    // Set game bounds ...
    let bounds = new Phaser.Rectangle(
        0,
        0,
        2000,
        2000,
    );

    // Set seed ...
    eurecaServer.log(gameSeed);
    Math.seedrandom(gameSeed);

    //  Resize our game world to be a 2000 x 2000 square
    game.world.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
	game.stage.disableVisibilityChange = true;
	
    //  Our tiled scrolling background
    land = game.add.tileSprite(0, 0, 800, 600, "grid");
    land.fixedToCamera = true;

    generateObstacles(bounds);
    let spawnPoint = getSpawnPoint(bounds);

    tanksList = {};

	player = new Tank(myId, game, tank);
    player.revive();

	tanksList[myId] = player;
	tank = player.tank;
	turret = player.turret;
	tank.x = spawnPoint.x;
	tank.y = spawnPoint.y;
	bullets = player.bullets;

    //  Explosion pool
    explosions = game.add.group();
    for (var i = 0; i < 10; i ++)
    {
        var explosionAnimation = explosions.create(0, 0, "explosion", [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add("explosion");
    }

    deathGraphic = game.add.sprite(game.width / 2, game.height / 2, "deathGraphic");
    deathGraphic.anchor.setTo(0.5);
    deathGraphic.fixedToCamera = true;
    deathGraphic.visible = false;
    deathGraphic.bringToTop();
    game.add.tween(deathGraphic).to(
        {y: deathGraphic.y + 50},
        1000,
        Phaser.Easing.Quadratic.InOut,
        true,
    ).repeat(-1);

    game.camera.follow(tank);
    game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
    game.camera.focusOnXY(0, 0);

    cursors = game.input.keyboard.createCursorKeys();

}

function update () {
    if (!ready) return;

	player.input.left = cursors.left.isDown;
	player.input.right = cursors.right.isDown;
	player.input.up = cursors.up.isDown;
    player.input.down = cursors.down.isDown;
	player.input.fire = game.input.activePointer.isDown;
	player.input.tx = game.input.x+ game.camera.x;
	player.input.ty = game.input.y+ game.camera.y;

	turret.rotation = game.physics.arcade.angleToPointer(turret);	
    land.tilePosition.x = -game.camera.x;
    land.tilePosition.y = -game.camera.y;

    game.physics.arcade.collide(obstacles, tank, tankHitObstacle);
	
    for (let i in tanksList) {
        if (tanksList.hasOwnProperty(i)) {
            game.physics.arcade.collide(obstacles, tanksList[i].bullets, bulletHitObstacle);
        }
    }

    for (var i in tanksList)
    {
		if (!tanksList[i]) continue;
		var curBullets = tanksList[i].bullets;
		var curTank = tanksList[i].tank;
		for (var j in tanksList)
		{
			if (!tanksList[j]) continue;
			if (j!=i)
			{
			
				var targetTank = tanksList[j].tank;
				
                // This interesting hack lets me see the full tank object on the bulletHitPlayer end ...
				game.physics.arcade.overlap(curBullets, targetTank, (tank, bullet) => {
                    bulletHitPlayer(tanksList[j], bullet);
                }, null, this);

			}
			if (tanksList[j].alive)
			{
				tanksList[j].update();
			}			
		}
    }
}

function deathGraphicShow() {
    deathGraphic.scale.setTo(0);
    deathGraphic.visible = true;
    game.add.tween(deathGraphic.scale).to(
        {x: 1, y: 1},
        800,
        Phaser.Easing.Quadratic.Out,
        true,
    );
    game.time.events.add(respawnTime, () => {
        deathGraphic.visible = false;
    });
}

function tankHitObstacle() {
    player.collisionDetected = true;
}

function bulletHitObstacle(obstacle, bullet) {
    bullet.kill();
    explosionAt(bullet.x, bullet.y);
}

function bulletHitPlayer (tank, bullet) {
    bullet.kill();
    explosionAt(bullet.x, bullet.y);

    let decreaseBy = Math.random() * 10;

    // In the end, this works because we needed the parent tank object so we could see the HP,
    // but we needed the id too! We had the id when we were just looking at the tank but we lost
    // the id ...
    eurecaServer.updateHealth(decreaseBy, tank.hp - decreaseBy <= 0, tank.tank.id);
}

function explosionAt(x, y) {
    let exp = explosions.getFirstDead();
    exp.position.setTo(x, y);
    exp.revive();
    exp.animations.play("explosion", 30);
    exp.scale.setTo(1 + (2 * Math.random()));
    exp.rotation = (2 * Math.PI) * Math.random();
    exp.animations.currentAnim.onComplete.add(exp.kill, exp);
}