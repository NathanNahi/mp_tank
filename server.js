var express = require('express');
var EurecaServer = require('eureca.io').EurecaServer;
var app = express(app);
var server = require('http').createServer(app);

// Set the server seed ...
var seed = "lalala";

app.use(express.static(__dirname));

var eurecaServer = new EurecaServer({allow: [
    'setId',
    'setSeed',
    'spawnEnemy',
    'kill',
    'revive',
    'updateState',
    'updateHealth'
]});
var clients = {};

eurecaServer.attach(server);

eurecaServer.onConnect(function (conn){
    console.log('New Client id=%s ', conn.id, conn.remoteAddress);

    var remote = eurecaServer.getClient(conn.id);

    clients[conn.id] = {id: conn.id, remote: remote};

    remote.setSeed(seed);
    remote.setId(conn.id);
});

eurecaServer.onDisconnect(function (conn){
    console.log('Client disconnected id=%s', conn.id);

    var removeId = clients[conn.id].id;

    delete clients[conn.id].id;

    for (var c in clients) {
        var remote = clients[c].remote;

        remote.kill(conn.id);
    }
});

eurecaServer.exports.handshake = function() {
    for (var c in clients) {
        var remote = clients[c].remote;
        for (var cc in clients) {

            var x = clients[cc].laststate ? clients[cc].laststate.x: 0;
            var y = clients[cc].laststate ? clients[cc].laststate.y: 0;

            remote.spawnEnemy(clients[cc].id, x, y);
        }
    }
}

eurecaServer.exports.handleState = function (state) {
    var conn = this.connection;
    var updatedClient = clients[conn.id];

    for (var c in clients) {
        var remote  = clients[c].remote;
        remote.updateState(updatedClient.id, state);

        clients[c].laststate = state;
    }
}

eurecaServer.exports.log = function(string) {
    console.log("Server:", string);
}

eurecaServer.exports.updateHealth = function(by, killed, id) {
    var conn = this.connection; // I think this is set to whoever called the method? ...
    var updatedClient = clients[conn.id];

    console.log(id, "took", by, "dmg!");

    for (var c in clients) {
        var remote  = clients[c].remote;
        remote.updateHealth(id, by);
    }

    if (killed) {
        console.log(id, "killed in battle!");
        for (var c in clients) {
            var remote  = clients[c].remote;
            remote.kill(id);
        }
    }
}

eurecaServer.exports.revivePlayer = function(id) {
    var conn = this.connection;
    var updatedClient = clients[conn.id];

    console.log("Server: Attempting to revive player " + id);

    for (var c in clients) {
        // Does this include the conn remote? ...
        var remote  = clients[c].remote;
        remote.revive(id);
    }
}

server.listen(8000);